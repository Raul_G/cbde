import java.io.IOException;
import java.util.Vector;
import java.util.ArrayList;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.client.HBaseAdmin;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.hbase.mapreduce.TableMapper;
import org.apache.hadoop.hbase.mapreduce.TableReducer;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.KeyValue;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.hbase.mapreduce.TableSplit;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.util.ToolRunner;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;

// INPUT

///TABLE 1////////////////////////////////////////////

//        key1              a:a      value=10

//        key2              a:a      value=20

//        key3              a:a      value=30

/////////////////////////////////////////////////////

///TABLE 2///////////////////////////////////////////
//
//        key1              b:b      value=10
//                          c:c      value=200
//
//        key2              b:b      value=20
//
//        key3              b:b      value=30
//
//        key4              b:b      value=10
//
////////////////////////////////////////////////////

//////EXPECTED QUERY => yarn jar RA.jar Join inputTable1 inputTable2 outputTable a b

// OUTPUT
////////////////////////////////////////////////////
//        key1_key1             a:a      value=10
//                              b:b      value=10
//                              c:c      value=200
//
//        key1_key4             a:a      value=10
//                              b:b      value=10
//      
//        key2_key2             a:a      value=20
//                              b:b      value=20
//
//        key3_key3             a:a      value=30
//                              b:b      value=30
////////////////////////////////////////////////////

public class Join extends Configured implements Tool {
    public static String inputTable1;
    public static String inputTable2;
    private static String outputTable;
    private static String leftAttribute;
    private static String rightAttribute;

    public static void main(String[] args) throws Exception {
        // La query ha d'introduir els 5 paràmetres requerits
        if (args.length != 5) {
            System.err.println("Parameters missing: inputTable1 inputTable2 outputTable leftAttribute rightAttribute");
            System.exit(1);
        }

        // Assignem l'entrada de cada paràmetre a una variable prèviament declarada
        inputTable1 = args[0];
        inputTable2 = args[1];
        outputTable = args[2];
        leftAttribute = args[3];
        rightAttribute = args[4];

        // Es comprova que les taules siguin vàlides amb una crida al checkIOTables
        int tablesRight = checkIOTables(args);
        if (tablesRight == 0) {

            // Executem l'algoritme amb una crida al run
            int ret = ToolRunner.run(new Join(), args);
            System.exit(ret);
        } else {
            System.exit(tablesRight);
        }
    }

    private static int checkIOTables(String [] args) throws Exception {
        Configuration config = HBaseConfiguration.create();
        HBaseAdmin hba = new HBaseAdmin(config);

        // Input table 1 ha d'existir
        if (!hba.tableExists(inputTable1)) {
            System.err.println("L'input table 1 no existeix");
            return 2;
        }
        // Input table 2 ha d'existir
        if (!hba.tableExists(inputTable2)) {
            System.err.println("L'input table 2 no existeix");
            return 2;
        }
        // Output table NO ha d'existir
        if (hba.tableExists(outputTable)) {
            System.err.println("L'output table ja existeix");
            return 3;
        }

        // Declarem descriptors de les taules per poder assignar les seves famílies a l'output
        HTableDescriptor htdInput1 = hba.getTableDescriptor(inputTable1.getBytes());
        HTableDescriptor htdInput2 = hba.getTableDescriptor(inputTable2.getBytes());

        // Inicialitzem la taula d'output
        HTableDescriptor htdOutput = new HTableDescriptor(outputTable.getBytes());

        // Assignem a l'output les mateixes famílies que la input table 1
        for (byte[] key: htdInput1.getFamiliesKeys())
            htdOutput.addFamily(new HColumnDescriptor(key));

        // Assignem a l'output les mateixes famílies que la input table 2
        for (byte[] key: htdInput2.getFamiliesKeys())
            if (!htdOutput.hasFamily(key))
                htdOutput.addFamily(new HColumnDescriptor(key));

        hba.createTable(htdOutput);
        return 0;
    }

    public int run(String [] args) throws Exception {

        // Creem la configuració de l'HBase
        Job job = new Job(HBaseConfiguration.create());
        job.setJarByClass(Join.class);
        job.setJobName("Join");

        // Passem una de les taules d'input com a externa i una altra com a externa per treballar de la mateixa forma que amb el producte cartesià:
        // La taula externa envia cada tupla a UN sol reducer, definit per una funció de hash.
        // La taula interna envia TOTES les tuples a TOTS els reducers.
        job.getConfiguration().setStrings("External", inputTable1);
        job.getConfiguration().setStrings("Internal", inputTable2);
        job.getConfiguration().setStrings("LeftAttribute", leftAttribute);
        job.getConfiguration().setStrings("RightAttribute", rightAttribute);
        job.getConfiguration().setInt("Hash", 2);

        // Necessitam un array de 2 Scans: un per cada input table
        ArrayList<Scan> scans = new ArrayList<Scan>();

        Scan scan1 = new Scan();
        scan1.setAttribute("scan.attributes.table.name", Bytes.toBytes(inputTable1));
        scans.add(scan1);

        Scan scan2 = new Scan();
        scan2.setAttribute("scan.attributes.table.name", Bytes.toBytes(inputTable2));
        scans.add(scan2);

        // Init map and reduce functions.
        TableMapReduceUtil.initTableMapperJob(scans, Mapper.class, Text.class, Text.class, job);
        TableMapReduceUtil.initTableReducerJob(outputTable, Reducer.class, job);

        boolean success = job.waitForCompletion(true);
        return success ? 0 : 1;
    }

    public static class Mapper extends TableMapper<Text, Text> {

        public void map(ImmutableBytesWritable rowMetadata, Result values, Context context) throws IOException, InterruptedException {

            int i;
            int hash = context.getConfiguration().getInt("Hash", 0);

            String[] external = context.getConfiguration().getStrings("External", "Default");
            String[] internal = context.getConfiguration().getStrings("Internal", "Default");

            // Del input agafem el TableSplit que correspon a aquesta fila
            TableSplit currentSplit = (TableSplit)context.getInputSplit();

			// Obtenim el nom de la taula a la que pertany per poder diferenciar entre l'interna i l'externa
            TableName tableNameB = currentSplit.getTable();
            String tableName = tableNameB.getQualifierAsString();

            // Creem un String per cada key: tableName#key;family:attributeValue.
            String tuple = tableName + "#" + new String(rowMetadata.get(), "US-ASCII");

            KeyValue[] attributes = values.raw();
            for (i = 0; i < attributes.length; ++i) {
                tuple = tuple + ";" + new String(attributes[i].getFamily()) + ":" + new String(attributes[i].getQualifier()) + ":" + new String(attributes[i].getValue());
            }

            // Taula inputTable1 o externa
            if (tableName.equalsIgnoreCase(external[0])) {
                // En aquest cas, el hash value el passem hardcodejat perquè no volem que sigui un paràmetre
				// Aquesta clau només es escrita una vegada
                context.write(new Text(Integer.toString(Double.valueOf(Math.random()*hash).intValue())), new Text(tuple));
            }
            // Taula inputTable2 o interna
            if (tableName.equalsIgnoreCase(internal[0])) {
                // Se escriu varies vegades, en funció del valor de hash
                for (int x = 0; x < hash; x++) {
                    context.write(new Text(Integer.toString(x)), new Text(tuple));
                }
            }
        }
    }

    public static class Reducer extends TableReducer<Text, Text, Text> {

        public void reduce(Text key, Iterable<Text> inputList, Context context) throws IOException, InterruptedException {
            int i, j, k;
            Put put;
            String outputKey;

            String[] external = context.getConfiguration().getStrings("External", "Default");
            String[] internal = context.getConfiguration().getStrings("Internal", "Default");
            String[] leftAttribute = context.getConfiguration().getStrings("LeftAttribute", "Default");
            String[] rightAttribute = context.getConfiguration().getStrings("RightAttribute", "Default");

            String eTableTuple, iTableTuple;
            String eTuple, iTuple;
            String[] eAttributes, iAttributes;
            String[] attribute_value;

            // Guardem totes les tuples que tinguin el mateix valor de hash 
            Vector<String> tuples = new Vector<String>();
            for (Text val : inputList) {
                tuples.add(val.toString());
            }

            // Es fa el join de cada tupla interna amb cada tupla externa  
            for (i = 0; i < tuples.size(); ++i){
                eTableTuple = tuples.get(i);
                eTuple = eTableTuple.split("#")[1];
                eAttributes = eTuple.split(";");

                if (eTableTuple.startsWith(external[0])) {

                    for (j = 0; j < tuples.size(); ++j) {
                        iTableTuple = tuples.get(j);
                        iTuple = iTableTuple.split("#")[1];
                        iAttributes = iTuple.split(";");

                        if (iTableTuple.startsWith(internal[0])) {

                            //Crear key per al output
                            outputKey = eAttributes[0] + "_" + iAttributes[0];

                            //Crear tupla
                            put = new Put(outputKey.getBytes());

                            //Strings per guardar el valors per les columnes
                            String leftValue = null;
                            String rightValue = null;

                            // Posem valors a les columnes de la taula externa
                            for (k = 1; k < eAttributes.length; ++k) {
                                attribute_value = eAttributes[k].split(":");

                                // Guardem el valor si coincideix amb el que estem buscant al join
                                if (attribute_value[1].equals(leftAttribute[0]))
                                    leftValue = attribute_value[2];

                                put.addColumn(attribute_value[0].getBytes(), attribute_value[1].getBytes(), attribute_value[2].getBytes());
                            }

                            // Ara posem valors a la taula interna
                            for (k = 1; k < iAttributes.length; ++k) {
                                attribute_value = iAttributes[k].split(":");

                                // Igual que abans, mirem si el valor coincideix
                                if (attribute_value[1].equals(rightAttribute[0]))
                                    rightValue = attribute_value[2];

                                put.addColumn(attribute_value[0].getBytes(), attribute_value[1].getBytes(), attribute_value[2].getBytes());
                            }

                            //Guardem la tupla a la taula de output
                            //Nomes el guardem si els dos valors son iguals i no nulls
                            if (leftValue != null && rightValue != null && leftValue.equals(rightValue)){
                                context.write(new Text(outputKey), put);
                            }
                        }
                    }
                }
            }
        }
    }
}