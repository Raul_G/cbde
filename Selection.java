import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.KeyValue;
import org.apache.hadoop.hbase.client.HBaseAdmin;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.hbase.mapreduce.TableMapper;
import org.apache.hadoop.hbase.mapreduce.TableReducer;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import java.io.IOException;

// Exemple de selecció

// INPUT

//        key1          a:a       valor=10

//        key2          a:a       valor=12
//                      b:b       valor=30

//        key3          a:a       valor=10
//                      c:c       valor=40

// yarn jar RA.jar Selection alexserravidalInput alexserravidalOutput a 10
// Per tant, seleccionem les files que tinguin una familia:columna {a:a} amb valor 10

// OUTPUT
//        key1          a:a       valor=10

//        key3          a:a       valor=10
//                      c:c       valor=40

public class Selection extends Configured implements Tool {
    private static String inputTable;
    private static String outputTable;

    public static void main(String[] args) throws Exception {
        // Comprovem que els paràmetres rebuts siguin 4
        if (args.length != 4) {
            System.err.println("Parameters missing: inputTable outputTable [family:]attribute value");
            System.exit(1);
        }

        // Assignem els paràmetres de la query a variables
        inputTable = args[0];
        outputTable = args[1];

        // Comprovem que les taules siguin vàlides
        int tablesRight = checkIOTables(args);

        // Si les taules són vàlides, executem l'algoritme
        if (tablesRight == 0) {
            int ret = ToolRunner.run(new Selection(), args);
            System.exit(ret);
        } 

        else {
            System.exit(tablesRight);
        }
    }

    private static int checkIOTables(String [] args) throws Exception {
        Configuration config = HBaseConfiguration.create();
        HBaseAdmin hba = new HBaseAdmin(config);

        // Comprovem que la taula d'input existeixi
        if (!hba.tableExists(inputTable)) {
            System.err.println("Input table does not exist");
            return 2;
        }
        // Comprovem que la taula d'output NO existeixi
        if (hba.tableExists(outputTable)) {
            System.err.println("Output table already exists");
            return 3;
        }

        // Cridem al descriptor de la taula d'Input, creem el de l'Output
        HTableDescriptor htdInput = hba.getTableDescriptor(inputTable.getBytes());
        HTableDescriptor htdOutput = new HTableDescriptor(outputTable.getBytes());

        // Fem que la taula d'output tingui la mateixa estructrua que la d'input. Per acabar, la creem.
        for (byte[] familyKey : htdInput.getFamiliesKeys())
            htdOutput.addFamily(new HColumnDescriptor(familyKey));
        hba.createTable(htdOutput);

        return 0;
    }

    public int run(String [] args) throws Exception {
        // Creem la configuració de l'HBase
        Job job = new Job(HBaseConfiguration.create());
        job.setJarByClass(Selection.class);
        job.setJobName("Selection");

        Scan scan = new Scan();

        // Passem a la configuració els atributs necessaris per executar la query, i ho assignem a un "attributes" que podrem cridar des del Mapper i el Reducer
        String header = args[2] + "," + args[3];
        job.getConfiguration().setStrings("attributes", header);

        // Inicialitzem les funcions de map i de reduce
        TableMapReduceUtil.initTableMapperJob(inputTable, scan, Mapper.class, Text.class, Text.class, job);
        TableMapReduceUtil.initTableReducerJob(outputTable, Reducer.class, job);

        boolean success = job.waitForCompletion(true);
        return success ? 0 : 4;
    }

    public static class Mapper extends TableMapper<Text, Text> {

        public void map(ImmutableBytesWritable rowMetadata, Result values, Context context) throws IOException, InterruptedException {

            String rowId = new String(rowMetadata.get(), "US-ASCII");

            // Agafem args[2] = familia:columna i args[3] = valor de la selecció, necessaris per executar la query
            String[] attributes = context.getConfiguration().getStrings("attributes", "empty");

            // Attribute correspon a familia:columna sobre on fem el select, value correspon a la condició de selecció
            String attribute = attributes[0];
            String value = attributes[1];

            // Inicialitzem una variable attributeValue, que conté el valor de la família attribute per a la row on es passa el Mapper
            // Es processa diferent, depenent de si l'atribut ve definit en la forma "a:a" o "a"
            String attributeValue;
            if (attribute.contains(":")) {
                String[] familyColumn = attribute.split(":");
                attributeValue = new String(values.getValue(familyColumn[0].getBytes(), familyColumn[1].getBytes()));
            }
            else{
                attributeValue = new String(values.getValue(attribute.getBytes(), attribute.getBytes()));
			}
            // Es comprova que els valors coincideixin amb el valor que s'ha passat com a paràmetre en la query (value). En cas afirmatiu, s'emmagatzemen totes
            // les columnes d'aquesta row, i es passen al context, que s'enviarà al reducer.
            if (attributeValue.equals(value)) {

                KeyValue[] raw = values.raw();
                String tuple = new String(raw[0].getFamily()) + ":" + new String(raw[0].getValue());

                for (int i = 1; i < raw.length; i++){
                    tuple += ";" + new String(raw[i].getFamily()) + ":" + new String(raw[i].getValue());
				}
                context.write(new Text(rowId), new Text(tuple));
            }
        }
    }

    public static class Reducer extends TableReducer<Text, Text, Text> {

        public void reduce(Text key, Iterable<Text> inputList, Context context) throws IOException, InterruptedException {


            //Input list és una llista que conté totes les tuples que el reducer ha de tractar. En aquest cas ja són totes les tuples que la query necessita
            //En un select, el reducer no ha de fer cap mena de feina
            for(Text outputKey : inputList) {				
                //key té la key de la row que s'està tractant en aquest moment
                //outputKey te tots els conjunts familia -> valor de la row que s'està tractant

                Put put = new Put(key.getBytes());
                for (String row : outputKey.toString().split(";")) {
                    String[] values = row.split(":");
                    // Afegim la família, la columna i el valor. La família i la columna tenen el mateix valor.
                    put.add(values[0].getBytes(), values[0].getBytes(), values[1].getBytes());
                }
                context.write(outputKey, put);
            }
        }
    }
}