// Standard classes
import java.io.IOException; 
import java.util.Vector; 
import java.util.ArrayList; 

// HBase classes
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.client.HBaseAdmin;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.hbase.mapreduce.TableMapper;
import org.apache.hadoop.hbase.mapreduce.TableReducer;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.KeyValue;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.hbase.mapreduce.TableSplit;
import org.apache.hadoop.hbase.TableName;

// Hadoop classes
import org.apache.hadoop.util.ToolRunner;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Reducer.Context;

public class GroupBy extends Configured implements Tool {

    // These are two global variables, which coincide with the three parameters in the call
    public static String inputTable;
    private static String outputTable;


//=================================================================== Main
        /* 
            Explanation: Queremos hacer el GroupBy sobre un atributo de una tabla guardada en HBase
            
            Primero, el metodo main tiene que comprobar los parametros. Tienen que haber 4:
            1.- Tabla de input HBase de donde leer los datos (tiene que existir)
			2.- Tabla donde se guardara el resultado (se creara, no puede existir)
            3.- Nombre de la columna con la que hacer group by
            4.- Atributo que hay que agregar
            
            Asume una llamada asi:

            yarn jar myJarFile.jar GroupBy Username_InputTable Username_OutputTable a b
            
            Asume las siguientes tablas HBase:
			
			- La tabla HBase Username_InputTable1 conteniendo los siguientes datos:
				'key1', 'a:a', '10'
				'key1', 'b:b', '20'
				'key2', 'a:a', '20'
				'key2', 'b:b', '30'
				'key2', 'c:c', '15'
				'key3', 'a:a', '10'
				'key3', 'b:b', '100'
			
			- La tabla HBase Username_OutputTable contendra los siguientes datos:
				'a:10', 'b:120'
				'a:20', 'b:30'
            
            Queremos juntar todos los valores b, agrupandolos por la familia a.
            Main llama a checkIOTables, la cual conecta con HBase y la gestiona.
            Finalmente, la tarea MapReduce se ejecuta.
       */

    public static void main(String[] args) throws Exception {
        if (args.length < 4) {
            System.err.println("Parameters missing: 'inputTable outputTable groupByAttribute aggregateAttribute'");
            System.exit(1);
        }
        inputTable = args[0];
        outputTable = args[1];

        int tablesRight = checkIOTables(args);
        if (tablesRight == 0) {
            int ret = ToolRunner.run(new GroupBy(), args);
            System.exit(ret);
        } else {
            System.exit(tablesRight);
        }
    }


//============================================================== checkTables

    // Explanation: Handles HBase and its relationship with the MapReduce task

    private static int checkIOTables(String[] args) throws Exception {
        // Obtain HBase's configuration
        Configuration config = HBaseConfiguration.create();
        // Create an HBase administrator
        HBaseAdmin hba = new HBaseAdmin(config);

         /* With an HBase administrator we check if the input table exists.
            You can modify this bit and create it here if you want. */
        if (!hba.tableExists(inputTable)) {
            System.err.println("Input table does not exist");
            return 2;
        }
         /* Check whether the output table exists. 
            Just the opposite as before, we do create the output table.
            Normally, MapReduce tasks attack an existing table (not created on the fly) and 
            they store the result in a new table. */
        if (hba.tableExists(outputTable)) {
            System.err.println("Output table already exists");
            return 3;
        }

        // Get an HBase descriptor pointing at the new table
        HTableDescriptor htdOutput = new HTableDescriptor(outputTable.getBytes());

        //Anadir familia del atributo de agregacion
        htdOutput.addFamily(new HColumnDescriptor(args[3]));

        //Create the new output table
        hba.createTable(htdOutput);
        return 0;
    }


    //============================================================== Job config
    //Create a new job to execute. This is called from the main and starts the MapReduce job
    public int run(String[] args) throws Exception {

        //Create a new MapReduce configuration object.
        Job job = new Job(HBaseConfiguration.create());
        //Set the MapReduce class
        job.setJarByClass(GroupBy.class);
        //Set the job name
        job.setJobName("GroupBy");
        //Create an scan object
        Scan scan = new Scan();
        //Set the columns to scan and keep header to project
        String header = args[2] + "," + args[3];
        job.getConfiguration().setStrings("attributes", header);
        //Set the Map and Reduce function
        TableMapReduceUtil.initTableMapperJob(inputTable, scan, Mapper.class, Text.class, Text.class, job);
        TableMapReduceUtil.initTableReducerJob(outputTable, Reducer.class, job);

        boolean success = job.waitForCompletion(true);
        return success ? 0 : 4;
    }


//=================================================================== Mapper

       /* This is the mapper class, which implements the map method. 
          The MapReduce framework will call this method automatically when needed.
          Note its header defines the input key-value and an object where to store the resulting key-values produced in the map.
          - ImmutableBytesWritable rowMetadata: The input key
          - Result values: The input value associated to that key
          - Context context: The object where to store all the key-values generated (i.e., the map output) */

    public static class Mapper extends TableMapper<Text, Text> {

        public void map(ImmutableBytesWritable rowMetadata, Result values, Context context) throws IOException, InterruptedException {
            String[] attributes = context.getConfiguration().getStrings("attributes", "default");

            String groupBy = attributes[0];
            String aggregate = attributes[1];

            //Nombre de familia y qualifier son iguales
			String groupByValue = new String(values.getValue(groupBy.getBytes(), groupBy.getBytes()));
            String aggregateValue = new String(values.getValue(aggregate.getBytes(), aggregate.getBytes()));
			context.write(new Text(groupByValue), new Text(aggregateValue));
            /*En el map output juntamos todos los valores que tengan el valor groupBy como nombre de familia, con los que
              tienen aggregateValue como qualifier. Siguiendo el ejemplo del principio [a, {20, 25}], sacado de las filas:
              'key1', 'a:a', '10'
			  'key1', 'b:b', '20'
			  'key1', 'b:b', '25'
			  etcetera.
			*/
            
        }
    }

    //================================================================== Reducer
    public static class Reducer extends TableReducer<Text, Text, Text> {

      /* The reduce is automatically called by the MapReduce framework after the Merge Sort step.
      It receives a key, a list of values for that key, and a context object where to write the resulting key-value pairs. */

        public void reduce(Text key, Iterable<Text> inputList, Context context) throws IOException, InterruptedException {
            Put put;
            String outputKey;
            String[] attributes = context.getConfiguration().getStrings("attributes", "");

            String groupBy = attributes[0];
            String aggregate = attributes[1];

            //Por cada key (nombre de familia groupBy con valor diferente) sumamos la lista de valores de la columna
            //de agregacion.

            int suma = 0;
            for (Text val : inputList) {
                suma += Integer.valueOf(val.toString());
            }

            outputKey = groupBy + "=" + key.toString();
            put = new Put(outputKey.getBytes());
            put.add(aggregate.getBytes(), aggregate.getBytes(), String.valueOf(suma).getBytes());
			context.write(new Text(outputKey), put);
        }
    }
}

